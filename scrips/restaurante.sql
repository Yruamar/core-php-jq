-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-05-2019 a las 22:52:55
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `restaurante`
--
CREATE DATABASE IF NOT EXISTS `restaurante` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `restaurante`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catcategoriasrestaurante`
--

CREATE TABLE `catcategoriasrestaurante` (
  `IdCategoria` int(11) NOT NULL,
  `Nombre` varchar(150) NOT NULL,
  `Descripcion` varchar(254) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catcategoriasrestaurante`
--

INSERT INTO `catcategoriasrestaurante` (`IdCategoria`, `Nombre`, `Descripcion`) VALUES
(1, 'Mexicana', 'Comida mexicana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catrestaurantes`
--

CREATE TABLE `catrestaurantes` (
  `IdRestaurante` int(11) NOT NULL,
  `NombreRestaurante` varchar(60) NOT NULL,
  `Alias` varchar(60) NOT NULL,
  `Domicilio` varchar(240) NOT NULL,
  `Telefono_1` varchar(20) NOT NULL,
  `Telefono_2` varchar(20) NOT NULL,
  `Correo` varchar(100) NOT NULL,
  `PaginaWeb` varchar(120) NOT NULL,
  `Facebook` varchar(150) NOT NULL,
  `Twitter` varchar(150) NOT NULL,
  `Sucursal` varchar(100) NOT NULL,
  `IdCategoriaRestaurante` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `catcategoriasrestaurante`
--
ALTER TABLE `catcategoriasrestaurante`
  ADD PRIMARY KEY (`IdCategoria`);

--
-- Indices de la tabla `catrestaurantes`
--
ALTER TABLE `catrestaurantes`
  ADD PRIMARY KEY (`IdRestaurante`),
  ADD KEY `IdCategoriaRestaurante` (`IdCategoriaRestaurante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `catcategoriasrestaurante`
--
ALTER TABLE `catcategoriasrestaurante`
  MODIFY `IdCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `catrestaurantes`
--
ALTER TABLE `catrestaurantes`
  MODIFY `IdRestaurante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
