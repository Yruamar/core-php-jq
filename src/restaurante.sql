-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 14-05-2019 a las 03:21:39
-- Versión del servidor: 5.7.23
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `restaurante`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catcategoriasrestaurante`
--

DROP TABLE IF EXISTS `catcategoriasrestaurante`;
CREATE TABLE IF NOT EXISTS `catcategoriasrestaurante` (
  `IdCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(150) NOT NULL,
  `Descripcion` varchar(254) NOT NULL,
  PRIMARY KEY (`IdCategoria`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catcategoriasrestaurante`
--

INSERT INTO `catcategoriasrestaurante` (`IdCategoria`, `Nombre`, `Descripcion`) VALUES
(1, 'Mexicana', 'Comida mexicana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catrestaurantes`
--

DROP TABLE IF EXISTS `catrestaurantes`;
CREATE TABLE IF NOT EXISTS `catrestaurantes` (
  `IdRestaurante` int(11) NOT NULL AUTO_INCREMENT,
  `NombreRestaurante` varchar(60) NOT NULL,
  `Alias` varchar(60) NOT NULL,
  `Domicilio` varchar(240) NOT NULL,
  `Telefono_1` varchar(20) NOT NULL,
  `Telefono_2` varchar(20) NOT NULL,
  `Correo` varchar(100) NOT NULL,
  `PaginaWeb` varchar(120) NOT NULL,
  `Facebook` varchar(150) NOT NULL,
  `Twitter` varchar(150) NOT NULL,
  `Sucursal` varchar(100) NOT NULL,
  `IdCategoriaRestaurante` int(11) NOT NULL,
  PRIMARY KEY (`IdRestaurante`),
  KEY `IdCategoriaRestaurante` (`IdCategoriaRestaurante`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catrestaurantes`
--

INSERT INTO `catrestaurantes` (`IdRestaurante`, `NombreRestaurante`, `Alias`, `Domicilio`, `Telefono_1`, `Telefono_2`, `Correo`, `PaginaWeb`, `Facebook`, `Twitter`, `Sucursal`, `IdCategoriaRestaurante`) VALUES
(1, 'Tacos paco', 'El gran Taco', 'ALicia', '123123', '123123', 'Correo', 'miPagina.com', 'facebook.com', 'twitter.com', 'La grande', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
