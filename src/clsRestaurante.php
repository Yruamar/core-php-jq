<?php

class clsRestaurante{
    private $idRestaurante=0;
    private $nombreRestaurante ="";
    private $alias ="";
    private $domicilio ="";
    private $telefono_1 ="";
    private $telefono_2 ="";
    private $correo ="";
    private $paginaWeb ="";
    private $facebook ="";
    private $sucursal ="";
    private $twitter ="";
    private $IdCategoriaRestaurante =0;

    
    public   $objConexion ="";
function __construct(conexionBD $coneccion) {
    
    try{
        $this->objConexion=$coneccion;

    }catch(Exception $e){
        die();
    }
}
function mdConsultarTodos(){
    try{
        $imprimir ="<table border=1>";

        $query =  "SELECT `IdRestaurante`, `NombreRestaurante`, `Alias`
                            , `Domicilio`, `Telefono_1`, `Telefono_2`, `Correo`
                            , `PaginaWeb`, `Facebook`, `Twitter`, `Sucursal`
                            , `IdCategoriaRestaurante` FROM `catrestaurantes`";
 

        
        $sth = $this->objConexion->conn->prepare($query);
        $sth->execute();
        while ($result = $sth->fetch(PDO::FETCH_ASSOC)) {
            $spanEliminar = "<span class='eliminar' data-id='{$result['IdRestaurante']}' >Eliminar</span>";
            $spanEditar = "<span class='editar' data-id='{$result['IdRestaurante']}' >Editar</span>";

            $imprimir .= "<tr>";
            $imprimir .="<td>{$result['NombreRestaurante']}</td>";
            $imprimir .="<td>{$result['Correo']}</td>";
            $imprimir .="<td>$spanEliminar -  $spanEditar</td>";
            $imprimir .= "</tr>";
            
        }
        $imprimir .= "</table>";
        echo $imprimir;
        

    }catch(Exception $e){
        die();
    }


    
}
function crear($nombreRestaurante, $alias,$domicilio,$telefono_1,$telefono_2,$correo
                ,$paginaWeb,$facebook,$sucursal,$twitter,$IdCategoriaRestaurante) {

    try{
        $this->nombreRestaurante = $nombreRestaurante;
        $this->alias = $alias;
        $this->domicilio = $domicilio;
        $this->telefono_1 = $telefono_1;
        $this->telefono_2 = $telefono_2;
        $this->correo = $correo;
        $this->paginaWeb = $paginaWeb;
        $this->facebook = $facebook;
        $this->sucursal = $sucursal;
        $this->twitter = $twitter;
        $this->IdCategoriaRestaurante = $IdCategoriaRestaurante;
        

        $query =  "INSERT INTO `catrestaurantes`(`NombreRestaurante`, `Alias`, `Domicilio`
                            , `Telefono_1`, `Telefono_2`, `Correo`, `PaginaWeb`, `Facebook`, `Twitter`, `Sucursal`, `IdCategoriaRestaurante`) 
                            VALUES (:nombreRes,:alias,:domicilio,:tele_1,:tele_2,:correo,:web,:facebook,:twiter,:sucursal,:IdCategoriaRestaurante)";
 

        
        $sth = $this->objConexion->conn->prepare($query);
        $sth->bindParam(':nombreRes', $this->nombreRestaurante);
        $sth->bindParam(':alias', $this->alias);
        $sth->bindParam(':domicilio', $this->domicilio);
        $sth->bindParam(':tele_1', $this->telefono_1);
        $sth->bindParam(':tele_2', $this->telefono_2);
        $sth->bindParam(':correo', $this->correo);
        $sth->bindParam(':web', $this->paginaWeb);
        $sth->bindParam(':facebook', $this->facebook);
        $sth->bindParam(':twiter', $this->twitter);
        $sth->bindParam(':sucursal', $this->sucursal);
        $sth->bindParam(':IdCategoriaRestaurante', $this->IdCategoriaRestaurante);
        $sth->execute();


        $this->objConexion->mtdCerrarConexion($sth);

    }catch(Exception $e){
        die();
    }
}
function actualizar($nombreRestaurante, $alias,$domicilio,$telefono_1,$telefono_2,$correo
            ,$paginaWeb,$facebook,$sucursal,$twitter,$IdCategoriaRestaurante,$idRestaurante) {

    try{
        $this->idRestaurante=$idRestaurante;
        $this->nombreRestaurante = $nombreRestaurante;
        $this->alias = $alias;
        $this->domicilio = $domicilio;
        $this->telefono_1 = $telefono_1;
        $this->telefono_2 = $telefono_2;
        $this->correo = $correo;
        $this->paginaWeb = $paginaWeb;
        $this->facebook = $facebook;
        $this->sucursal = $sucursal;
        $this->twitter = $twitter;
        $this->IdCategoriaRestaurante = $IdCategoriaRestaurante;
        


        $query =  "UPDATE `catrestaurantes` 
        SET 
        `NombreRestaurante`=:nombreRes
        ,`Alias`=:alias,`Domicilio`=:domicilio
        ,`Telefono_1`=:tele_1,`Telefono_2`=:tele_2
        ,`Correo`=:correo,`PaginaWeb`=:web
        ,`Facebook`=:facebook,`Twitter`=:twiter
        ,`Sucursal`=:sucursal,`IdCategoriaRestaurante`=:IdCategoriaRestaurante
         where IdRestaurante=:idRestaurante";

        
        $sth = $this->objConexion->conn->prepare($query);
        $sth->bindParam(':idRestaurante', $this->idRestaurante);
        $sth->bindParam(':nombreRes', $this->nombreRestaurante);
        $sth->bindParam(':alias', $this->alias);
        $sth->bindParam(':domicilio', $this->domicilio);
        $sth->bindParam(':tele_1', $this->telefono_1);
        $sth->bindParam(':tele_2', $this->telefono_2);
        $sth->bindParam(':correo', $this->correo);
        $sth->bindParam(':web', $this->paginaWeb);
        $sth->bindParam(':facebook', $this->facebook);
        $sth->bindParam(':twiter', $this->twitter);
        $sth->bindParam(':sucursal', $this->sucursal);
        $sth->bindParam(':IdCategoriaRestaurante', $this->IdCategoriaRestaurante);
        $sth->execute();


        $this->objConexion->mtdCerrarConexion($sth);

    }catch(Exception $e){
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        die();
    }
}

function mdConsultarUno($id){
    try
    {
        $query =  "SELECT `IdRestaurante`, `NombreRestaurante`, `Alias`
        , `Domicilio`, `Telefono_1`, `Telefono_2`, `Correo`
        , `PaginaWeb`, `Facebook`, `Twitter`, `Sucursal`
        , `IdCategoriaRestaurante` FROM `catrestaurantes` where IdRestaurante=:IdRest";

    $sth = $this->objConexion->conn->prepare($query);
    $sth->bindParam(':IdRest', $id);
    $sth->execute();
    
    $convertToJson = array();
    while ($result = $sth->fetch(PDO::FETCH_ASSOC)) {
        $rowArray['IdRestaurante'] = $result['IdRestaurante'];
        $rowArray['NombreRestaurante'] = $result['NombreRestaurante'];
        $rowArray['Alias'] = $result['Alias'];
        $rowArray['Domicilio'] = $result['Domicilio'];
        $rowArray['Telefono_1'] = $result['Telefono_1'];
        $rowArray['Telefono_2'] = $result['Telefono_2'];
        $rowArray['Correo'] = $result['Correo'];
        $rowArray['PaginaWeb'] = $result['PaginaWeb'];
        $rowArray['Facebook'] = $result['Facebook'];
        $rowArray['Twitter'] = $result['Twitter'];
        $rowArray['Sucursal'] = $result['Sucursal'];
        $rowArray['IdCategoriaRestaurante'] = $result['IdCategoriaRestaurante'];
      
        array_push($convertToJson, $rowArray);
    }
    
    return  json_encode($convertToJson);

    

    } catch (PDOException $e) {
        echo $e;
        die();
       
    }catch(Exception $error){
        echo $error;
        die();
    }

    
}
function __destruct() {
    $this->objConexion=null;
}

}
?>