<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script>
$(function(){
    llenarTodos();
    llenarSelect();
    $("#formularioRestaurante")[0].reset();





    $("#contenedorTabla").on('click',".editar",function(){
        let id= $(this).data('id');
        let formData = new FormData();
        formData.append("condicion", "consultarUno");
        formData.append("idRestaurante",id);
        $.ajax({
                url:'funciones.php',        
                type:'POST',       
                data: formData,
                async:false,
                processData: false,
                contentType: false,
                type:  'post',
                beforeSend: function () {
                },
                success: function(json){
                 console.log(json);
                    json=$.parseJSON(json);
                            
                            $(json).each(function(i,val){
                                 arreglo=[];
                                 $.each(val,function(k,v){
                                     arreglo[k]=v;
                                 });
        
                                 $("#idRestaurante").val(arreglo["IdRestaurante"]);
                                 $("#NombreRestaurante").val(arreglo["NombreRestaurante"]);
                                 $("#Alias").val(arreglo["Alias"]);
                                 $("#Domicilio").val(arreglo["Domicilio"]);
                                 $("#Telefono_1").val(arreglo["Telefono_1"]);
                                 $("#Telefono_2").val(arreglo["Telefono_2"]);
                                 $("#Correo").val(arreglo["Correo"]);
                                 $("#PaginaWeb").val(arreglo["PaginaWeb"]);
                                 $("#Facebook").val(arreglo["Facebook"]);
                                 $("#Twitter").val(arreglo["Twitter"]);
                                 $("#Sucursal").val(arreglo["Sucursal"]);
                                 $("#tipoRestaurante").val(arreglo["IdCategoriaRestaurante"]);
                                
                             });

                },
                complete: function() {
                    // mtdcargarPagina();
                }
            });
    })
    $("#formularioRestaurante").submit(function(){
        try{
            var formData = new FormData();
            if($("#idRestaurante").val()==0){
                formData.append("condicion", "crearRestaurante"); 
            }else{
                formData.append("condicion", "Actualizar"); 
                formData.append("idRestaurante", $("#idRestaurante").val()); 
            }
            formData.append("NombreRestaurante",$("#NombreRestaurante").val());
            formData.append("Alias",$("#Alias").val());
            formData.append("Domicilio",$("#Domicilio").val());
            formData.append("Telefono_1",$("#Telefono_1").val());
            formData.append("Telefono_2",$("#Telefono_2").val());
            formData.append("Correo",$("#Correo").val());
            formData.append("PaginaWeb",$("#PaginaWeb").val());
            formData.append("Facebook",$("#Facebook").val());
            formData.append("Twitter",$("#Twitter").val());
            formData.append("Sucursal",$("#Sucursal").val());
            formData.append("tipoRestaurante",$("#tipoRestaurante").val());
            
            $.ajax({
                url:'funciones.php',        
                type:'POST',       
                data: formData,
                async:false,
                processData: false,
                contentType: false,
                type:  'post',
                beforeSend: function () {
                },
                success: function(json){
                        console.log(json);
                },
                complete: function() {
                    llenarTodos();
                    $(this)[0].reset();
                }
            });

        
        return false;
        }catch(ex){
            console.log(ex.message);
            return false;
        
        }
    })



})
function llenarSelect(){
        //llenar select
        try{
            var formData = new FormData();

            formData.append("condicion", "consultarSelect"); 
            $.ajax({
                url:'funciones.php',        
                type:'POST',       
                data: formData,
                async:false,
                processData: false,
                contentType: false,
                type:  'post',
                beforeSend: function () {
                },
                success: function(json){
                    $("#tipoRestaurante").html(json);
                

                },
                complete: function() {
                    // mtdcargarPagina();
                }
            });
        }
            catch(ex){
            console.log(ex.message);
        }

    }
    function llenarTodos(){
        //llenar select
        try{
            var formData = new FormData();

            formData.append("condicion", "consultarRestaurantes"); 
            $.ajax({
                url:'funciones.php',        
                type:'POST',       
                data: formData,
                async:false,
                processData: false,
                contentType: false,
                type:  'post',
                beforeSend: function () {
                },
                success: function(json){
                    $("#contenedorTabla").html(json);
                },
                complete: function() {
                    // mtdcargarPagina();
                }
            });
        }
            catch(ex){
            console.log(ex.message);
        }

    }


</script>
</head>
<body>
<div id="contenedorTabla">
</div>
<form id="formularioRestaurante">
<input type="hidden" id="idRestaurante" required >
    <label for="">Nombre</label>
    <input type="text" id="NombreRestaurante" required >
    <br>
    <label for="">Alias</label>
    <input type="text" id="Alias" required >
    <br>
    <label for="">Domicilio</label>
    <input type="text" id="Domicilio" required >
    <br>
    <label for="">Telefono_1</label>
    <input type="text" id="Telefono_1" required >
    <br>
    <label for="">Telefono_2</label>
    <input type="text" id="Telefono_2" required >
    <br>
    <label for="">Correo</label>
    <input type="text" id="Correo" required >
    <br>
    <label for="">PaginaWeb</label>
    <input type="text" id="PaginaWeb" required >
    <br>
    <label for="">Facebook</label>
    <input type="text" id="Facebook" required >
    <br>
    <label for="">Twitter</label>
    <input type="text" id="Twitter" required >
    <br>
    <label for="">Sucursal</label>
    <input type="text" id="Sucursal" required >
    <br>
    <select name="" id="tipoRestaurante">
    </select>
 <p><input type="submit" /> <button type="reset">Reset</button></p>
</form>
</body>
</html>