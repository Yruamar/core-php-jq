<?php


class conexionBD{
    
    private $usuario    = "";
    private $contrasena = "";
    private $servidor   = "";
    private $baseDatos  = "";

    private $sintaxisMotorBD= "";
    private $sintaxisNombreBD= "";
    public $conn       = "";


    private $procedimientoEjecutarComienzo = "";
    private $procedimientoEJecutarFinal    = "";

    function  __construct($usuario,$contrasena, $servidor, $baseDatos,$motoBD){
       
        try 
        {
            $this->usuario=$usuario;
            $this->contrasena=$contrasena;
            $this->servidor=$servidor;
            $this->baseDatos=$baseDatos; 
            if($motoBD==1)
            {
                $this->sintaxisMotorBD               ="sqlsrv:Server";
                $this->sintaxisNombreBD              ="Database";
                $this->procedimientoEjecutarComienzo = "exec ";
            }else{
                $this->sintaxisMotorBD ="mysql:host";
                $this->sintaxisNombreBD ="dbname";
                $this->procedimientoEjecutarComienzo = "Call (";
                $this->procedimientoEJecutarFinal    = " )";
            }
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            die();
        }  

    }

    function mtdconectar(){
        try {
        $this->conn   = new PDO( $this->sintaxisMotorBD."=".$this->servidor.";".$this->sintaxisNombreBD."=".$this->baseDatos, $this->usuario, $this->contrasena, array(\PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8')); 
        // Configurar ATTR_EMULATE_PREPARES a false puede ser importante ya que le dice a PDO que deshabilite los
        //  prepared statements emulados y sólo utilice los de verdad (por ejemplo en caso de que el driver no pueda
        //   preparar satisfactoriamente la sentencia).
        // EVITA INYECCIONES SQL
        $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        # Activación del modo de trabajo de PDO
        $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );
        $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

        // Se recomienda activar esta opción para gestionar los errores con PDOException
        $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
        // return  $this->conn;
        } catch (PDOException $e) {
            print "¡Error!: " . $e->getMessage() . "<br/>";
            die();
        }

    } 

    function mtdConsultar($con){
        try {
                $sth =  $con->prepare("SELECT * from catPdoInsertar");
                $sth->execute();
                echo "<table border=1><tr>";
                $result = $sth->fetchAll(PDO::FETCH_ASSOC);
                $keys = array_keys($result[0]);
                foreach ($keys as $key)
                  echo "<th>".$key."</th>";
                echo "</tr>";
                foreach ($result as $row) {
                  echo "<tr>";
                  foreach ($keys as $key)
                      echo "<td>".$row[$key]."</td>";
                  echo "</tr>";
                }
                echo "</table>";
        } catch (PDOException $e) {
            print "¡Error!: " . $e->getMessage() . "<br/>";
            die();
        }

    } 
    function mtdUsarSP($spEjecuta){
        try {

            $sentencia=$this->procedimientoEjecutarComienzo." ".$spEjecuta.$this->procedimientoEJecutarFinal;

            return $sentencia;
        } catch (Exception $e) {
            $con->rollBack();
            print "¡Error!: " . $e->getMessage() . "<br/>";
           
        }

    }
    function mtdCerrarConexion($sth){
        try{
            $sth = null;
            $this->conn= null;
        }catch (Exception $e) {
            print "¡Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    } 
    


}


// $coneccionSQL = new coneccionBD("sa","Amaury.-1324","DESKTOP-BRB6FBS","Prueba",1);
// $cone = $coneccionSQL->mtdconectar();
// $coneccionSQL->mtdUsarSP($cone,"Hola, esto es una prue---baxs");
// $coneccionSQL->mtdConsultar($cone);

?>